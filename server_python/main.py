import socket

UDP_PORT = 29152
UDP_BUFFER = 1025

SERVER_IP4 = "0.0.0.0"
SERVER_IP6 = "::"

def decode_udp(udp_packet):
     pass

if __name__ == '__main__':
    
    sock4 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock4.bind((SERVER_IP4, UDP_PORT))
    
    # sock6 = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
    # sock6.bind((SERVER_IP6, UDP_PORT))
    
    print("Start Server")
    counter = 0
    
    while True:
        try:
            udp_packet = sock4.recv(UDP_BUFFER)
            print("Recived Packet {2}: Addr: {0}, Data: {1}".format(udp_packet[0], udp_packet[1], counter))
            decode_udp(udp_packet)
            counter = counter + 1
                        
        except KeyboardInterrupt:
            print("Stop Server")
            exit(0)
﻿using DIoT;
using DIoT.Encoding;
using DIoT.Encryption;
using DIoT.Header;
using DIoT.Schema;
using System.Net.Sockets;

namespace DIoT_Client
{
    internal class Program
    {
        static int UDP_PORT = 29152;

        static void Main(string[] args)
        {
            byte[] key = Helper.ByteFromString("16352f16e9b04cdf9f2df669f60ffdb4");
            UInt32 messageCounter = 123;
            UInt32 deviceAddress = 1001;
            byte schemaAddress = 2;

            while (true)
            {
                List<byte> packet = new();

                BaseHeader baseHeader = new BaseHeader();
                baseHeader.Version = 1;
                baseHeader.Type = DIoT.Packtes.PacketTypes.Type.DATA;

                DataHeader dataHeader = new DataHeader();
                dataHeader.DeviceAddress = deviceAddress;
                dataHeader.MessageCounter = messageCounter;

                SchemaHeader schemaHeader = new SchemaHeader();
                schemaHeader.SchemaAddress = schemaAddress;
                schemaHeader.HasError = false;

                SchemaLoader schemaLoader = new SchemaLoader();
                schemaLoader.DeserializeJson(File.ReadAllText("./Test2.diot"));

                PayloadEncoder payloadEncoder = new(schemaLoader.Schemata[0]);
                payloadEncoder.SetValue(0, (byte)195);
                payloadEncoder.SetValue(1, (UInt32)4278190335);
                payloadEncoder.SetValue(2, true);

                byte[] payload = payloadEncoder.GetPayload();
                byte[] payloadWithHeader = new byte[payload.Length + SchemaHeader.GetHeaderSize()];
                Array.Copy(payload, 0, payloadWithHeader, SchemaHeader.GetHeaderSize(), payload.Length);
                Array.Copy(schemaHeader.ExportData(), 0, payloadWithHeader, 0, SchemaHeader.GetHeaderSize());

                PayloadEncrypter payloadEncrypter = new PayloadEncrypter();
                byte[] encryptedPayload = payloadEncrypter.encrypt(key, messageCounter, dataHeader.ExportData(), payloadWithHeader);

                packet.AddRange(baseHeader.ExportData());
                packet.AddRange(dataHeader.ExportData());
                packet.AddRange(encryptedPayload);

                UdpClient udpClient = new UdpClient();
                udpClient.Send(packet.ToArray(), packet.Count, "127.0.0.1", UDP_PORT);
                Console.WriteLine($"Send packet: {messageCounter}");

                Console.WriteLine("Press any key to send next packet");
                Console.ReadKey();
                messageCounter++;
            }
        }
    }
}
﻿using System.Globalization;

namespace DIoT
{
    public static class Helper
    {
        public static string GetBitStringMSB(byte[] data)
        {
            string s = "";
            for(int i = 0; i < data.Length; i++)
            {
                s += Convert.ToString(data[i], 2).PadLeft(8, '0');
            }
            return s;
        }

        public static byte[] ByteFromString(string hexString)
        {
            hexString = hexString.Replace("-", "");

            if (hexString.Length % 2 != 0)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "The binary key cannot have an odd number of digits: {0}", hexString));
            }

            byte[] data = new byte[hexString.Length / 2];
            for (int index = 0; index < data.Length; index++)
            {
                string byteValue = hexString.Substring(index * 2, 2);
                data[index] = byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            }

            return data;
        }
    }
}

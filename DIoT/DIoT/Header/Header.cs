﻿using Org.BouncyCastle.Tls;

namespace DIoT.Header
{
    public abstract class Header
    {
        public Header(byte[] data)
        {
            LoadData(data);
        }

        public Header()
        {

        }

        public abstract void LoadData(byte[] data);

        public abstract byte[] ExportData();

        public static int GetHeaderSize()
        {
            return 0;
        }
    }
}

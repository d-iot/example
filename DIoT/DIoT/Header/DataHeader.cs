﻿namespace DIoT.Header
{
    public class DataHeader : Header
    {
        public UInt32 DeviceAddress;
        public UInt32 MessageCounter;
        public DataHeader(byte[] data) : base(data)
        {

        }

        public DataHeader() : base()
        {

        }

        public override void LoadData(byte[] data)
        {
            if (data.Length != GetHeaderSize())
            {
                throw new ArgumentException("Data is invalid");
            }

            DeviceAddress = BitConverter.ToUInt32(data, 0);
            MessageCounter = BitConverter.ToUInt32(data, 4);
        }

        public override byte[] ExportData()
        {
            byte[] data = new byte[GetHeaderSize()];
            BitConverter.GetBytes(DeviceAddress).CopyTo(data, 0);
            BitConverter.GetBytes(MessageCounter).CopyTo(data, 4);

            return data;
        }

        public static new int GetHeaderSize()
        {
            return 8;
        }
    }
}

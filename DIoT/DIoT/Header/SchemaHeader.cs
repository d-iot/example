﻿namespace DIoT.Header
{
    public class SchemaHeader : Header 
    {
        public byte SchemaAddress;
        public bool HasError;

        public SchemaHeader(byte[] data) : base(data)
        {

        }

        public SchemaHeader () : base()
        {

        }

        public override void LoadData(byte[] data)
        {
            if (data.Length != GetHeaderSize())
            {
                throw new ArgumentException("Data is invalid");
            }

            SchemaAddress = (byte)(data[0] & 0b_0111_1111);
            HasError = data[0] > 127;
        }

        public override byte[] ExportData()
        {
            if (SchemaAddress > 0b_0111_1111)
            {
                throw new ArgumentException("SchemaAddress is invalid");
            }

            byte[] data = new byte[GetHeaderSize()];
            data[0] = SchemaAddress;
            if(HasError)
            {
                data[0] |= 0b_1000_0000;
            }

            return data;
        }

        public static new int GetHeaderSize()
        {
            return 1;
        }
    }
}

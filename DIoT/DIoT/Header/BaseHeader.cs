﻿using DIoT.Packtes;

namespace DIoT.Header
{
    public class BaseHeader : Header
    {
        public byte Version;
        public PacketTypes.Type Type;

        public BaseHeader(byte[] data) : base(data)
        {

        }

        public BaseHeader() : base()
        {

        }

        public override void LoadData(byte[] data)
        {
            if (data.Length != GetHeaderSize())
            {
                throw new ArgumentException("Data is invalid");
            }

            Version = (byte)(data[0] >> 5);
            Type = PacketTypes.GetType((byte)(data[0] & 0b_0001_1111));
        }

        public override byte[] ExportData()
        {
            if((int)Type > 0b_0001_1111)
            {
                throw new ArgumentException("Type is invalid");
            }

            if(Version > 0b_0000_0111)
            {
                throw new ArgumentException("Version is invalid");
            }

            byte[] data = new byte[GetHeaderSize()];
            data[0] = (byte)(Version << 5);
            data[0] |= (byte)Type;

            return data;
        }

        public static new int GetHeaderSize()
        {
            return 1;
        }
    }
}

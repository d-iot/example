﻿using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Parameters;

namespace DIoT.Encryption
{
    public class PayloadDecrypter
    {
        public byte[] decrypt(byte[] key, byte[] nonce, byte[] associatedData, byte[] cipherData)
        {
            AsconEngine cipher = new AsconEngine(AsconEngine.AsconParameters.ascon128a);
            int macSize = 8 * cipher.GetKeyBytesSize();
            AeadParameters keyParamAead = new AeadParameters(new KeyParameter(key), macSize, nonce, associatedData);
            cipher.Init(false, keyParamAead);
            int outputSize = cipher.GetOutputSize(cipherData.Length);
            byte[] plainData = new byte[outputSize];
            int result = cipher.ProcessBytes(cipherData, 0, cipherData.Length, plainData, 0);
            cipher.DoFinal(plainData, result);
            return plainData;
        }

        public byte[] decrypt(byte[] key, UInt32 counter, byte[] associatedData, byte[] plainData)
        {
            byte[] nonce = new byte[16];
            byte[] c = BitConverter.GetBytes(counter);
            c.CopyTo(nonce, 0);
            
            return decrypt(key, nonce, associatedData, plainData);
        }
    }
}

﻿using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Parameters;

namespace DIoT.Encryption
{
    public class PayloadEncrypter
    {
        public byte[] encrypt(byte[] key, byte[] nonce, byte[] associatedData, byte[] plainData)
        {
            AsconEngine cipher = new AsconEngine(AsconEngine.AsconParameters.ascon128a);
            int macSize = 8 * cipher.GetKeyBytesSize();
            AeadParameters keyParamAead = new AeadParameters(new KeyParameter(key), macSize, nonce, associatedData);
            cipher.Init(true, keyParamAead);
            int outputSize = cipher.GetOutputSize(plainData.Length);
            byte[] cipherData = new byte[outputSize];
            int result = cipher.ProcessBytes(plainData, 0, plainData.Length, cipherData, 0);
            cipher.DoFinal(cipherData, result);
            return cipherData;
        }

        public byte[] encrypt(byte[] key, UInt32 counter, byte[] associatedData, byte[] plainData)
        {
            byte[] nonce = new byte[16];
            byte[] c = BitConverter.GetBytes(counter);
            c.CopyTo(nonce, 0);

            return encrypt(key, nonce, associatedData, plainData);
        }
    }
}

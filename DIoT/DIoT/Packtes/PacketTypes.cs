﻿namespace DIoT.Packtes
{
    public class PacketTypes
    {
        public enum Type
        {
            HELLO = 0,
            HELLO_RESPONSE = 1,
            DATA = 2
        }

        public static Type GetType(byte data)
        {
            if (data > 2)
            {
                throw new ArgumentException("Invalid Type");
            }
            return (Type)data;
        }
    }
}

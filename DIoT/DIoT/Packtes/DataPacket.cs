﻿namespace DIoT.Packtes
{
    public class DataPacket
    {
        public BasePaket BasePaket;
        public UInt32 DeviceAddress;
        public UInt32 MessageCounter;
        public byte[] Data;
        public byte[] MAC;

        public DataPacket(BasePaket basePaket)
        {
            BasePaket = basePaket;
            if(basePaket.Data.Length <= 4 + 4 + 0 + 16)
            {
                throw new ArgumentException("Data is invalid");
            }

            DeviceAddress = BitConverter.ToUInt32(BitOperation.SwitchEndianess(BitOperation.SubArray(basePaket.Data, 0, 4)));
            MessageCounter = BitConverter.ToUInt32(BitOperation.SwitchEndianess(BitOperation.SubArray(basePaket.Data, 4, 8)));
            Data = BitOperation.SubArray(basePaket.Data, basePaket.Data.Length + 8, basePaket.Data.Length - 4 + 4 + 16);
            MAC = BitOperation.SubArray(basePaket.Data, basePaket.Data.Length - 16 - 1, 16);
        }
    }
}

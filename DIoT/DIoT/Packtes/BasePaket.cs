﻿namespace DIoT.Packtes
{
    public class BasePaket
    {
        public int Version;
        public PacketTypes.Type Type;
        public byte[] Data;

        public BasePaket(byte[] data)
        {
            if (data.Length == 0)
            {
                throw new ArgumentException("Data is invalid");
            }

            Version = data[0] >> 5;
            Type = PacketTypes.GetType((byte)(data[0] & 0b_0001_1111));

            Data = data.Skip(1).ToArray();
        }

        public byte[] GetHeader()
        {
            byte[] data = new byte[1];
            data[0] = (byte)(((byte)Version) << 5);
            data[0] = (byte)(data[0] | (byte)Type);
            return data;
        }
    }
}

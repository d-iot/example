﻿namespace DIoT.Packtes
{
    public class HelloPacket
    {
        public BasePaket BasePacket;
        public Guid UUID;

        public HelloPacket(BasePaket basePacket) 
        {
            BasePacket = basePacket;
            if(basePacket.Data.Length != 16)
            {
                throw new ArgumentException("Packet is invalid");
            }
            UUID = new Guid(basePacket.Data);
        }
    }
}

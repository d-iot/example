using System.Text.Json.Serialization;
using Microsoft.VisualBasic.CompilerServices;

namespace DIoT.Schema;

public class Data
{
    [JsonPropertyName("id")]
    [JsonPropertyOrder(0)]
    public uint ID { get; set; } = 0;

    [JsonPropertyName("name")]
    [JsonPropertyOrder(1)]
    public string Name { get; set; } = "";

    [JsonPropertyName("description")]
    [JsonPropertyOrder(2)]
    public string Description { get; set; } = "";

    [JsonPropertyName("type")]
    [JsonPropertyOrder(3)]
    public DataTypes.Types Type { get; set; } = DataTypes.Types.RAW;

    [JsonPropertyName("array")]
    [JsonPropertyOrder(4)]
    public uint Array { get; set; } = 1;

    [JsonPropertyName("factor")]
    [JsonPropertyOrder(5)]
    public double Factor { get; set; } = 1.0;

    [JsonPropertyName("offset")]
    [JsonPropertyOrder(6)]
    public double Offset { get; set; } = 0;

    [JsonPropertyName("unit")]
    [JsonPropertyOrder(7)]
    public string Unit { get; set; } = "";

    [JsonPropertyName("enum")]
    [JsonPropertyOrder(9)]
    public Dictionary<string, double> Enum { get; set; } = new();

    [JsonIgnore]
    public byte[]? Value;
}
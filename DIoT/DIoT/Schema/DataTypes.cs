namespace DIoT.Schema;

public static class DataTypes
{
    public enum Types
    {
        RAW = 0,
    
        UINT8 = 1,
        UINT16 = 2,
        UINT32 = 3,
        UINT64 = 4,
    
        // RFU = 5,
    
        SINT8 = 6,
        SINT16 = 7,
        SINT32 = 8,
        SINT64 = 9,
    
        // RFU = 10,
    
        SFLOAT16 = 11,
        SFLOAT32 = 12,
        SFLOAT64 = 13,
    
        // RFU = 14,
    
        FLAG = 15
    }
    
    public static uint SizeInBit(Types type)
    {
        switch (type)
        {
            case Types.FLAG:
                return 1;

            case Types.RAW:
            case Types.SINT8:
            case Types.UINT8:
                return 8;

            case Types.SINT16:
            case Types.UINT16:
            case Types.SFLOAT16:
                return 16;

            case Types.SINT32:
            case Types.UINT32:
            case Types.SFLOAT32:
                return 32;

            case Types.SINT64:
            case Types.UINT64:
            case Types.SFLOAT64:
                return 64;

            default:
                throw new ArgumentException("Invalid DataType");
        }
    }
}
using System.Text.Json;
using System.Text.Json.Serialization;

namespace DIoT.Schema;

public class SchemaLoader
{
    [JsonPropertyName("schema")]
    public List<Schema> Schemata { get; set; } = new();

    public void DeserializeJson(string json)
    {
        JsonSerializerOptions options = new JsonSerializerOptions
        {
            Converters =
            {
                new JsonStringEnumConverter(null)
            }
        };
        try
        {
            SchemaLoader tmp = JsonSerializer.Deserialize<SchemaLoader>(json, options) ?? throw new ArgumentException("JSON is invalid");
            Schemata = tmp.Schemata;
        }
        catch (Exception ex) 
        {
            throw new ArgumentException("JSON is invalid", ex);
        }
        
    }

    public string SerializeJson()
    {
        JsonSerializerOptions options = new JsonSerializerOptions
        {
            WriteIndented = true,
            Converters =
            {
                new JsonStringEnumConverter(null)
            }
        };

        return JsonSerializer.Serialize(this, options);
    }

    public string Validate()
    {
        string errors = "";
        foreach (Schema s in Schemata)
        {
            errors += s.Validate();
        }

        return errors;
    }
}
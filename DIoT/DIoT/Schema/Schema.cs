using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace DIoT.Schema;

public class Schema
{
    [JsonPropertyName("uuid")]
    [JsonPropertyOrder(0)]
    public string UUID { get; set; } = "";
        
    [JsonPropertyName("name")]
    [JsonPropertyOrder(1)]
    public string Name { get; set; } = "";
    
    [JsonPropertyName("description")]
    [JsonPropertyOrder(2)]
    public string Description { get; set; } = "";

    [JsonPropertyName("data")]
    [JsonPropertyOrder(3)]
    public List<Data> Data { get; set; } = new();

    public string Validate()
    {
        string errors = "";
        
        // Check Schema Errors
        Regex regex_UUID = new Regex(@"^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$");
        if (!regex_UUID.IsMatch(UUID))
        {
            errors += ($"Schema UUID is invalid: {UUID}\n");
        }

        if (Name == "")
        {
            errors += "Schema Name is empty\n";
        }

        if (Description == "")
        {
            errors += "Schema Description is empty\n";
        }

        if (Data == null || Data.Count == 0)
        {
            errors += "Schema Data is empty\n";
        }

        // Check Data Errors
        if (Data != null)
        {
            bool[] ids = new bool[Data.Count];

            foreach (Data d in Data)
            {
                if (d.ID < 0 || d.ID > 65535)
                {
                    errors += $"Data ID is invalid: {d.ID}\n";
                    continue;
                }

                if (d.ID >= Data.Count)
                {
                    errors += $"Data ID is out of range: {d.ID}\n";
                }
                else
                {
                    if (ids[d.ID] == true)
                    {
                        errors += $"Data ID is duplicate: {d.ID}\n";
                    }
                    else
                    {
                        ids[d.ID] = true;
                    }
                }

                if (d.Name == "")
                {
                    errors += $"Data {d.ID} Name is empty\n";
                }

                if (d.Description == "")
                {
                    errors += $"Data {d.ID} Description is empty\n";
                }

                if (!Enum.IsDefined(typeof(DataTypes.Types), d.Type))
                {
                    errors += $"Data {d.ID} Type is invalid: {d.Type}\n";
                }
                
                if (d.Enum != null && d.Enum.Count > 0)
                {
                    if (d.Array != 1)
                    {
                        errors += $"Data {d.ID} Enum is used, but Array is not 1\n";
                    }
                    if (d.Factor != 1)
                    {
                        errors += $"Data {d.ID} Enum is used, but Factor is not 1\n";
                    }
                    if (d.Offset != 0)
                    {
                        errors += $"Data {d.ID} Enum is used, but Offset is not 0";
                    }
                    if (d.Unit != "")
                    {
                        errors += $"Data {d.ID} Enum is used, but Unit is not empty\n";
                    }
                }
                else
                {
                    if (d.Array == 0)
                    {
                        errors += $"Data {d.ID} Array is 0\n";
                    }

                    if (d.Array > 65535)
                    {
                        errors += $"Data {d.ID} Array is invalid: {d.Array}\n";
                    }

                    if (d.Factor == 0)
                    {
                        errors += $"Data {d.ID} Factor is 0\n";
                    }

                    if (d.Factor < float.MinValue || d.Factor > float.MaxValue)
                    {
                        errors += $"Data {d.ID} Factor is out of range: {d.Factor}\n";
                    }
                
                    if (d.Offset < float.MinValue || d.Offset > float.MaxValue)
                    {
                        errors += $"Data {d.ID} Offset is out of range: {d.Offset}\n";
                    }
                    if(d.Type == DataTypes.Types.FLAG && d.Array != 1)
                    {
                        errors += $"Data {d.ID} Array of Flags are not supported\n";
                    }
                }
            }

            for (int i = 0; i < Data.Count; i++)
            {
                if (ids[i] == false)
                {
                    errors += $"ID is missing: {i}\n";
                }
            }
        }
        
        return errors;
    }

    public uint PayloadSizeBit()
    {
        if (Data == null)
        {
            throw new NullReferenceException("Data is null");
        }

        if (Validate() != "")
        {
            throw new ArgumentException("Schema is invalid");
        }

        uint counter = 0;
        foreach (Data d in Data)
        {
            counter += DataTypes.SizeInBit((d.Type)) * d.Array;
        }

        return counter;
    }
}
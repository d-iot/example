﻿using DIoT.Packtes;
using System.Net;

namespace DIoT
{
    public class PacketProcessor
    {
        public Dictionary<UInt32, StateMachine> DeviceState = new();
        public Dictionary<Guid, UInt32> Devices = new();

        public void LoadStateMachine(StateMachine stateMachine)
        {
            if(DeviceState.ContainsKey(stateMachine.DeviceAddress))
            {
                throw new ArgumentException("Device allready registerd");
            }
            Devices.Add(stateMachine.Device, stateMachine.DeviceAddress);
            DeviceState.Add(stateMachine.DeviceAddress, stateMachine);
        }

        public void ProcessPacket(byte[] data, IPEndPoint deviceIP)
        {
            BasePaket baseHeader;

            if (data.Length == 0)
            {
                return; // Drop Packet
            }

            try
            {
                baseHeader = new(data);
            }
            catch
            {
                return; // Drop Packet
            }

            if(baseHeader.Type == PacketTypes.Type.HELLO)
            {
                try
                {
                    HelloPacket helloPacket = new(baseHeader);
                    if(!Devices.ContainsKey(helloPacket.UUID))
                    {
                        return; // Drop Packet
                    }
                    DeviceState[Devices[helloPacket.UUID]].ProcessHello(helloPacket, deviceIP);
                }
                catch
                {
                    return; // Drop Packet
                }
            }
            else if(baseHeader.Type == PacketTypes.Type.DATA)
            {
                try
                {
                    DataPacket dataPacket = new(baseHeader);
                    if(!DeviceState.ContainsKey(dataPacket.DeviceAddress))
                    {
                        return; // Drop Packet
                    }
                    DeviceState[dataPacket.DeviceAddress].ProcessData(dataPacket, deviceIP);
                }
                catch
                {
                    return; // Drop Packet
                }
            }
        }
    }
}

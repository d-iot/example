﻿using System.Collections;

namespace DIoT
{
    public static class BitOperation
    {
        public static byte[] SwitchEndianess(byte[] array)
        {
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse((array));
            }

            return array;
        }

        public static byte[] SubArray(byte[] array, int offset, int count)
        {
            return new ArraySegment<byte>(array, offset, count).ToArray();
        }

        public static byte Reverse(byte data)
        {
            byte result = 0x00;

            for (byte mask = 0x80; Convert.ToInt32(mask) > 0; mask >>= 1)
            {
                result = (byte)(result >> 1);

                var tmp = (byte)(data & mask);
                if (tmp != 0x00)
                {
                    result = (byte)(result | 0x80);
                }
            }

            return (result);
        }

        public static byte[] GetBytesBitArray(BitArray data)
        {
            byte[] array = new byte[(int)Math.Ceiling(data.Length / 8.0)];

            data.CopyTo(array, 0);
            array = array.Reverse().ToArray();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = Reverse(array[i]);
            }

            return array;
        }

        public static BitArray GetBitArrayBytes(byte[] data, int size)
        {
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = Reverse(data[i]);
            }
            data = data.Reverse().ToArray();

            BitArray array = new BitArray(data);
            array.Length = size;
            return array;
        }
    }
}

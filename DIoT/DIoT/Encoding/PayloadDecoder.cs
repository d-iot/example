﻿using DIoT.Schema;
using System.Collections;

namespace DIoT.Encoding
{
    public class PayloadDecoder
    {
        public readonly Schema.Schema Schema;

        private PayloadData[] _PayloadData;
        private int _FlagCounter;
        private int _PayloadLength;

        public PayloadDecoder(Schema.Schema schema)
        {
            if (schema.Validate() != "")
            {
                throw new ArgumentException("Schema is invalid");
            }
            Schema = schema;
            _PayloadData = new PayloadData[Schema.Data.Count];

            for (int i = 0; i < Schema.Data.Count; i++)
            {
                _PayloadData[Schema.Data[i].ID] = new PayloadData(Schema.Data[i].Type, (int)Schema.Data[i].Array);

                if (Schema.Data[i].Type == DataTypes.Types.FLAG)
                {
                    _FlagCounter++;
                }
                else
                {
                    _PayloadLength += _PayloadData[Schema.Data[i].ID].Data.Length;
                }
            }

            _PayloadLength += (int)Math.Ceiling(_FlagCounter / 8.0);
        }

        private BitArray GetBitArrayBytes(byte[] data, int size)
        {
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = Reverse(data[i]);
            }
            data = data.Reverse().ToArray();

            BitArray array = new BitArray(data);
            array.Length = size;
            return array;
        }

        private byte Reverse(byte data)
        {
            byte result = 0x00;

            for (byte mask = 0x80; Convert.ToInt32(mask) > 0; mask >>= 1)
            {
                result = (byte)(result >> 1);

                var tmp = (byte)(data & mask);
                if (tmp != 0x00)
                {
                    result = (byte)(result | 0x80);
                }
            }

            return (result);
        }

        public int SchemaSize()
        {
            return _PayloadLength;
        }

        public int SchemaErrorByteSize()
        {
            return (int)Math.Ceiling(_PayloadData.Length / 8.0);
        }

        public int SchemaSizeError(byte[] error)
        {
            BitArray errors = GetBitArrayBytes(error, error.Length);

            int byteCounter = 0;
            int flagCounter = 0;
            for (int i = 0; i < _PayloadData.Length; i++)
            {
                if (!errors[i])
                {
                    if (_PayloadData[i].Type == DataTypes.Types.FLAG)
                    {
                        flagCounter++;
                    }
                    else
                    {
                        byteCounter += _PayloadData[i].Data.Length;
                    }
                }
            }
            byteCounter += (int)Math.Ceiling(flagCounter / 8.0);

            return byteCounter;
        }

        public void SetPayload(byte[] data)
        {
            if (data.Length != _PayloadLength)
            {
                throw new ArgumentException("Payload size is invalid");
            }

            List<byte> payload = new List<byte>(data);

            BitArray flags = new BitArray(1);
            if (_FlagCounter > 0)
            {
                int flagBytes = (int)Math.Ceiling(_FlagCounter/8.0);
                flags = GetBitArrayBytes(payload.GetRange(payload.Count - flagBytes, flagBytes).ToArray(), _FlagCounter);
                payload.RemoveRange(payload.Count - flagBytes, flagBytes);
            }

            int flagCounter = 0;
            for (int i = 0; i < _PayloadData.Length; i++)
            {
                if (_PayloadData[i].Type == DataTypes.Types.FLAG && _FlagCounter > 0)
                {
                    _PayloadData[i].Flag = flags[flagCounter];
                    flagCounter++;
                }
                else
                {
                    _PayloadData[i].Data = payload.GetRange(0, _PayloadData[i].Data.Length).ToArray();
                    payload.RemoveRange(0, _PayloadData[i].Data.Length);
                }
            }
        }

        public void SetPayloadWithErrors(byte[] data)
        {
            List<byte> payload = new List<byte>(data);

            int errorBytes = (int)Math.Ceiling(_PayloadData.Length / 8.0);
            if(payload.Count <= errorBytes)
            {
                throw new ArgumentException("Payload size is invalid");
            }

            BitArray errors = GetBitArrayBytes(payload.GetRange(0, errorBytes).ToArray(), _PayloadData.Length);
            payload.RemoveRange(0, errorBytes);

            int byteCounter = 0;
            int flagCounter = 0;
            for(int i = 0; i < _PayloadData.Length; i++)
            {
                if (!errors[i])
                {
                    if(_PayloadData[i].Type == DataTypes.Types.FLAG)
                    {
                        flagCounter++;
                    }
                    else
                    {
                        byteCounter += _PayloadData[i].Data.Length;
                    }
                }
            }
            byteCounter += (int)Math.Ceiling(flagCounter / 8.0);
            
            if(byteCounter != payload.Count)
            {
                throw new ArgumentException("Payload size is invalid");
            }

            BitArray flags = new BitArray(1);
            if (flagCounter > 0)
            {
                int flagBytes = (int)Math.Ceiling(flagCounter / 8.0);
                flags = GetBitArrayBytes(payload.GetRange(payload.Count - 1 - flagBytes, payload.Count - 1).ToArray(), flagCounter);
                payload.RemoveRange(payload.Count - 1 - flagBytes, payload.Count - 1);
            }

            flagCounter = 0;
            for (int i = 0; i < _PayloadData.Length; i++)
            {
                if (errors[i] == true)
                {
                    _PayloadData[i].IsError = true;
                }
                else
                {
                    if (_PayloadData[i].Type == DataTypes.Types.FLAG && _FlagCounter > 0)
                    {
                        _PayloadData[i].Flag = flags[flagCounter];
                        flagCounter++;
                    }
                    else
                    {
                        _PayloadData[i].Data = payload.GetRange(0, _PayloadData[i].Data.Length).ToArray();
                        payload.RemoveRange(0, _PayloadData[i].Data.Length);
                    }
                }
            }
        }

        private void CheckValue(UInt16 id, DataTypes.Types type, int array)
        {
            if (id >= _PayloadData.Length)
            {
                throw new ArgumentException("ID is invalid");
            }

            if (_PayloadData[id].Type != type)
            {
                throw new ArgumentException("Type is invalid");
            }

            if (array == 0 && _PayloadData[id].Array != 1)
            {
                throw new ArgumentException("Array size is invalid");
            }
            else
            {
                if (_PayloadData[id].Array != array)
                {
                    throw new ArgumentException("Array size is invalid");
                }
            }
            if (_PayloadData[id].IsError == true)
            {
                throw new ArgumentException("Has error");
            }
        }

        public bool IsError(UInt16 id)
        {
            if (id >= _PayloadData.Length)
            {
                throw new ArgumentException("ID is invalid");
            }
            return _PayloadData[id].IsError;
        }
        private byte[] SwitchEndianess(byte[] array)
        {
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(array);
            }

            return array;
        }

        #region GetValue
        public bool GetValueBool(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.FLAG, 1);

            return _PayloadData[id].Flag;
        }

        public byte GetValueByte(UInt16 id)
        {
            if (id >= _PayloadData.Length)
            {
                throw new ArgumentException("ID is invalid");
            }

            if (!(_PayloadData[id].Type == DataTypes.Types.UINT8 || _PayloadData[id].Type == DataTypes.Types.RAW))
            {
                throw new ArgumentException("Type is invalid");
            }

            if (_PayloadData[id].IsError == true)
            {
                throw new ArgumentException("Has error");
            }

            return _PayloadData[id].Data[0];
        }

        public UInt16 GetValueUInt16(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.UINT16, 1);

            return BitConverter.ToUInt16(SwitchEndianess(_PayloadData[id].Data));
        }

        public UInt32 GetValueUInt32(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.UINT32, 1);

            return BitConverter.ToUInt32(SwitchEndianess(_PayloadData[id].Data));
        }

        public UInt64 GetValueUInt64(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.UINT64, 1);

            return BitConverter.ToUInt64(SwitchEndianess(_PayloadData[id].Data));
        }

        public sbyte GetValueSByte(UInt16 id)
        {
            if (id >= _PayloadData.Length)
            {
                throw new ArgumentException("ID is invalid");
            }

            if (_PayloadData[id].Type != DataTypes.Types.SINT8)
            {
                throw new ArgumentException("Type is invalid");
            }

            if (_PayloadData[id].IsError == true)
            {
                throw new ArgumentException("Has error");
            }

            return (sbyte)_PayloadData[id].Data[0];
        }

        public Int16 GetValueInt16(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.SINT16, 1);

            return BitConverter.ToInt16(SwitchEndianess(_PayloadData[id].Data));
        }

        public Int32 GetValueInt32(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.SINT32, 1);

            return BitConverter.ToInt32(SwitchEndianess(_PayloadData[id].Data));
        }

        public Int64 GetValueInt64(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.SINT64, 1);

            return BitConverter.ToInt64(SwitchEndianess(_PayloadData[id].Data));
        }

        public Half GetValueHalf(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.SFLOAT16, 1);

            return BitConverter.ToHalf(SwitchEndianess(_PayloadData[id].Data));
        }

        public float GetValueFloat(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.SFLOAT32, 1);

            return BitConverter.ToSingle(SwitchEndianess(_PayloadData[id].Data));
        }

        public double GetValueDouble(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.SFLOAT64, 1);

            return BitConverter.ToDouble(SwitchEndianess(_PayloadData[id].Data));
        }
        #endregion

        #region GetArray
        public byte[] GetArrayByte(UInt16 id)
        {
            if (id >= _PayloadData.Length)
            {
                throw new ArgumentException("ID is invalid");
            }

            if (!(_PayloadData[id].Type == DataTypes.Types.UINT8 || _PayloadData[id].Type == DataTypes.Types.RAW))
            {
                throw new ArgumentException("Type is invalid");
            }

            if (_PayloadData[id].Array == 1)
            {
                throw new ArgumentException("Array size is invalid");
            }

            if (_PayloadData[id].IsError == true)
            {
                throw new ArgumentException("Has error");
            }

            return _PayloadData[id].Data;
        }

        public UInt16[] GetArrayUInt16(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.FLAG, 0);

            UInt16[] array = new UInt16[_PayloadData[id].Array];
            for (int i = 0; i < _PayloadData[id].Array; i++)
            {
                array[i] = BitConverter.ToUInt16(SwitchEndianess(new ArraySegment<byte>(_PayloadData[id].Data, i*2, 2).ToArray()));
            }

            return array;
        }

        public UInt32[] GetArrayUInt32(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.UINT32, 0);

            UInt32[] array = new UInt32[_PayloadData[id].Array];
            for (int i = 0; i < _PayloadData[id].Array; i++)
            {
                array[i] = BitConverter.ToUInt32(SwitchEndianess(new ArraySegment<byte>(_PayloadData[id].Data, i * 4, 4).ToArray()));
            }

            return array;
        }

        public UInt64[] GetArrayUInt64(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.UINT64, 0);

            UInt64[] array = new UInt64[_PayloadData[id].Array];
            for (int i = 0; i < _PayloadData[id].Array; i++)
            {
                array[i] = BitConverter.ToUInt64(SwitchEndianess(new ArraySegment<byte>(_PayloadData[id].Data, i * 8, 8).ToArray()));
            }

            return array;
        }

        public sbyte[] GetArraySByte(UInt16 id)
        {
            if (id >= _PayloadData.Length)
            {
                throw new ArgumentException("ID is invalid");
            }

            if (_PayloadData[id].Type != DataTypes.Types.SINT8)
            {
                throw new ArgumentException("Type is invalid");
            }

            if (_PayloadData[id].Array == 1)
            {
                throw new ArgumentException("Array size is invalid");
            }

            if (_PayloadData[id].IsError == true)
            {
                throw new ArgumentException("Has error");
            }

            return (sbyte[])(Array)_PayloadData[id].Data;
        }

        public Int16[] GetArrayInt16(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.SINT16, 0);

            Int16[] array = new Int16[_PayloadData[id].Array];
            for (int i = 0; i < _PayloadData[id].Array; i++)
            {
                array[i] = BitConverter.ToInt16(SwitchEndianess(new ArraySegment<byte>(_PayloadData[id].Data, i * 2, 2).ToArray()));
            }

            return array;
        }

        public Int32[] GetArrayInt32(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.SINT32, 0);

            Int32[] array = new Int32[_PayloadData[id].Array];
            for (int i = 0; i < _PayloadData[id].Array; i++)
            {
                array[i] = BitConverter.ToInt32(SwitchEndianess(new ArraySegment<byte>(_PayloadData[id].Data, i * 4, 4).ToArray()));
            }

            return array;
        }

        public Int64[] GetArrayInt64(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.SINT64, 0);

            Int64[] array = new Int64[_PayloadData[id].Array];
            for (int i = 0; i < _PayloadData[id].Array; i++)
            {
                array[i] = BitConverter.ToInt64(SwitchEndianess(new ArraySegment<byte>(_PayloadData[id].Data, i * 8, 8).ToArray()));
            }

            return array;
        }

        public Half[] GetArrayHalf(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.SFLOAT16, 0);

            Half[] array = new Half[_PayloadData[id].Array];
            for (int i = 0; i < _PayloadData[id].Array; i++)
            {
                array[i] = BitConverter.ToHalf(SwitchEndianess(new ArraySegment<byte>(_PayloadData[id].Data, i * 2, 2).ToArray()));
            }

            return array;
        }

        public float[] GetArrayFloat(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.SFLOAT32, 0);

            float[] array = new float[_PayloadData[id].Array];
            for (int i = 0; i < _PayloadData[id].Array; i++)
            {
                array[i] = BitConverter.ToSingle(SwitchEndianess(new ArraySegment<byte>(_PayloadData[id].Data, i * 4, 4).ToArray()));
            }

            return array;
        }

        public double[] GetArrayDouble(UInt16 id)
        {
            CheckValue(id, DataTypes.Types.SFLOAT64, 0);

            double[] array = new double[_PayloadData[id].Array];
            for (int i = 0; i < _PayloadData[id].Array; i++)
            {
                array[i] = BitConverter.ToDouble(SwitchEndianess(new ArraySegment<byte>(_PayloadData[id].Data, i * 8, 8).ToArray()));
            }

            return array;
        }
        #endregion
    }
}

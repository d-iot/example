﻿using DIoT.Schema;
using System.Collections;

namespace DIoT.Encoding
{
    public  class PayloadEncoder
    {
        private readonly Schema.Schema _Schema;

        private PayloadData[] _PayloadData;
        private int _FlagCounter;

        public PayloadEncoder(Schema.Schema schema)
        {
            if (schema.Validate() != "")
            {
                throw new ArgumentException("Schema is invalid");
            }
            _Schema = schema;
            _PayloadData = new PayloadData[_Schema.Data.Count];

            for(int i = 0; i < _Schema.Data.Count; i++)
            {
                _PayloadData[_Schema.Data[i].ID] = new PayloadData(_Schema.Data[i].Type, (int)_Schema.Data[i].Array);
            
                if(_Schema.Data[i].Type == DataTypes.Types.FLAG)
                {
                    _FlagCounter++;
                }
            }
        }

        public byte[] GetPayload()
        {
            if (!IsPayloadComplete())
            {
                throw new ArgumentException("Payload is incomplete");
            }

            if (HasPayloadErrors())
            {
                throw new ArgumentException("Payload has errors");
            }

            List<byte> payload = new List<byte>();

            BitArray flag = new BitArray(_FlagCounter);
            int flagCounter = 0;
            for (int i = 0; i < _PayloadData.Length; i++)
            {
                if (_PayloadData[i].Type == DataTypes.Types.FLAG)
                {
                    flag[flagCounter] = _PayloadData[i].Flag;
                    flagCounter++;
                }
                else
                {
                    payload.AddRange(_PayloadData[i].Data);
                }
            }

            if (flag.Length > 0)
            {
                payload.AddRange(GetBytesBitArray(flag));
            }

            return payload.ToArray();
        }

        public byte[] GetPayloadWithErrors()
        {
            if (!IsPayloadComplete())
            {
                throw new ArgumentException("Payload is incomplete");
            }

            List<byte> payload = new List<byte>();
            int flagCounter = 0;
            if (HasPayloadErrors())
            {
                BitArray errors = new BitArray(_PayloadData.Length);
                for (int i = 0; i < _PayloadData.Length; i++)
                {
                    errors[i] = _PayloadData[i].IsError;
                    if (!_PayloadData[i].IsError && _PayloadData[i].Type == DataTypes.Types.FLAG)
                    {
                        flagCounter++;
                    }
                }
                payload.AddRange(GetBytesBitArray(errors));
            }
            else
            {
                throw new ArgumentException("Payload has no errors");
            }

            BitArray flag = new BitArray(flagCounter);
            flagCounter = 0;
            for (int i = 0; i < _PayloadData.Length; i++)
            {
                if (!_PayloadData[i].IsError)
                {
                    if(_PayloadData[i].Type == DataTypes.Types.FLAG)
                    {
                        flag[flagCounter] = _PayloadData[i].Flag;
                        flagCounter++;
                    }
                    else
                    {
                        payload.AddRange(_PayloadData[i].Data);
                    }
                }
            }
            
            if(flag.Length > 0)
            {
                payload.AddRange(GetBytesBitArray(flag));
            }

            return payload.ToArray();
        }

        private byte ReverseByte(byte data)
        {
            byte result = 0x00;

            for (byte mask = 0x80; Convert.ToInt32(mask) > 0; mask >>= 1)
            {
                result = (byte)(result >> 1);

                var tmp = (byte)(data & mask);
                if (tmp != 0x00)
                {
                    result = (byte)(result | 0x80);
                }
            }

            return (result);
        }

        private byte[] GetBytesBitArray(BitArray data)
        {
            byte[] array = new byte[(int)Math.Ceiling(data.Length / 8.0)];

            data.CopyTo(array, 0);
            array = array.Reverse().ToArray();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = ReverseByte(array[i]);
            }

            return array;
        }

        public void ResetPayload()
        {
            foreach(PayloadData d in _PayloadData) 
            { 
                d.IsError = false;
                d.IsSet = false;
                d.Data = new byte[d.Data.Length];
            }
        }

        public bool IsPayloadComplete()
        {
            foreach (PayloadData d in _PayloadData)
            {
                if (!d.IsSet)
                {
                    return false;
                }
            }

            return true;
        }

        public bool HasPayloadErrors()
        {
            foreach (PayloadData d in _PayloadData)
            {
                if (d.IsError)
                {
                    return true;
                }
            }

            return false;
        }

        public void SetError(UInt16 id)
        {
            if (id >= _PayloadData.Length)
            {
                throw new ArgumentException("ID is invalid");
            }
            _PayloadData[id].IsError = true;
            _PayloadData[id].IsSet = true;
        }

        private void CheckValue(UInt16 id, DataTypes.Types type, int array)
        {
            if (id >= _PayloadData.Length)
            {
                throw new ArgumentException("ID is invalid");
            }

            if (_PayloadData[id].Type != type)
            {
                throw new ArgumentException("Type is invalid");
            }

            if (array == 0 && _PayloadData[id].Array != 1)
            {
                throw new ArgumentException("Array size is invalid");
            }
            else
            {
                if (_PayloadData[id].Array != array)
                {
                    throw new ArgumentException("Array size is invalid");
                }
            }
        }

        private void SetBytes(UInt16 id, byte[] array)
        {
            _PayloadData[id].Data = array;
            _PayloadData[id].IsSet = true;
            _PayloadData[id].IsError = false;
        }

        private byte[] SwitchEndianess(byte[] array)
        {
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse((array));
            }

            return array;
        }

        #region SetValue
        public void SetValue(UInt16 id, bool value)
        {
            CheckValue(id, DataTypes.Types.FLAG, 1);

            _PayloadData[id].Flag = value;

            _PayloadData[id].IsSet = true;
            _PayloadData[id].IsError = false;
        }

        public void SetValue(UInt16 id, byte value)
        {
            if (id >= _PayloadData.Length)
            {
                throw new ArgumentException("ID is invalid");
            }

            if (!(_PayloadData[id].Type == DataTypes.Types.UINT8 || _PayloadData[id].Type == DataTypes.Types.RAW))
            {
                throw new ArgumentException("Type is invalid");
            }

            _PayloadData[id].Data = new byte[] { value };

            _PayloadData[id].IsSet = true;
            _PayloadData[id].IsError = false;
        }

        public void SetValue(UInt16 id, UInt16 value)
        {
            CheckValue(id, DataTypes.Types.UINT16, 1);
            SetBytes(id, SwitchEndianess(BitConverter.GetBytes(value)));
        }

        public void SetValue(UInt16 id, UInt32 value)
        {
            CheckValue(id, DataTypes.Types.UINT32, 1);
            SetBytes(id, SwitchEndianess(BitConverter.GetBytes(value)));
        }

        public void SetValue(UInt16 id, UInt64 value)
        {
            CheckValue(id, DataTypes.Types.UINT64, 1);
            SetBytes(id, SwitchEndianess(BitConverter.GetBytes(value)));
        }

        public void SetValue(UInt16 id, sbyte value)
        {
            CheckValue(id, DataTypes.Types.SINT8, 1);

            _PayloadData[id].Data = new byte[] { (byte)value };

            _PayloadData[id].IsSet = true;
            _PayloadData[id].IsError = false;
        }

        public void SetValue(UInt16 id, Int16 value)
        {
            CheckValue(id, DataTypes.Types.SINT32, 1);
            SetBytes(id, SwitchEndianess(BitConverter.GetBytes(value)));
        }

        public void SetValue(UInt16 id, Int32 value)
        {
            CheckValue(id, DataTypes.Types.SINT32, 1);
            SetBytes(id, SwitchEndianess(BitConverter.GetBytes(value)));
        }

        public void SetValue(UInt16 id, Int64 value)
        {
            CheckValue(id, DataTypes.Types.SINT64, 1);
            SetBytes(id, SwitchEndianess(BitConverter.GetBytes(value)));
        }

        public void SetValue(UInt16 id, Half value)
        {
            CheckValue(id, DataTypes.Types.SFLOAT16, 1);
            SetBytes(id, SwitchEndianess(BitConverter.GetBytes(value)));
        }

        public void SetValue(UInt16 id, float value)
        {
            CheckValue(id, DataTypes.Types.SFLOAT32, 1);
            SetBytes(id, SwitchEndianess(BitConverter.GetBytes(value)));
        }

        public void SetValue(UInt16 id, double value)
        {
            CheckValue(id, DataTypes.Types.SFLOAT64, 1);
            SetBytes(id, SwitchEndianess(BitConverter.GetBytes(value)));
        }

        #endregion

        #region SetArray
        public void SetArray(UInt16 id, byte[] value)
        {
            if (id >= _PayloadData.Length)
            {
                throw new ArgumentException("ID is invalid");
            }

            if (!(_PayloadData[id].Type == DataTypes.Types.UINT8 || _PayloadData[id].Type == DataTypes.Types.RAW))
            {
                throw new ArgumentException("Type is invalid");
            }

            if (_PayloadData[id].Array != value.Length)
            {
                throw new ArgumentException("Array size is invalid");
            }

            SetBytes(id, value);
        }

        public void SetArray(UInt16 id, UInt16[] value)
        {
            CheckValue(id, DataTypes.Types.UINT16, value.Length);
            byte[] array = new byte[value.Length * 2];
            for (int i = 0; i < value.Length; i++)
            {
                SwitchEndianess(BitConverter.GetBytes(value[i])).CopyTo(array, i * 2);
            }
            SetBytes(id, array);
        }

        public void SetArray(UInt16 id, UInt32[] value)
        {
            CheckValue(id, DataTypes.Types.UINT32, value.Length);
            byte[] array = new byte[value.Length * 4];
            for (int i = 0; i < value.Length; i++)
            {
                SwitchEndianess(BitConverter.GetBytes(value[i])).CopyTo(array, i * 4);
            }
            SetBytes(id, array);
        }

        public void SetArray(UInt16 id, UInt64[] value)
        {
            CheckValue(id, DataTypes.Types.UINT64, value.Length);
            byte[] array = new byte[value.Length * 8];
            for (int i = 0; i < value.Length; i++)
            {
                SwitchEndianess(BitConverter.GetBytes(value[i])).CopyTo(array, i * 8);
            }
            SetBytes(id, array);
        }

        public void SetArray(UInt16 id, sbyte[] value)
        {
            CheckValue(id, DataTypes.Types.SINT8, value.Length);
            SetBytes(id, (byte[])(Array)value);
        }

        public void SetArray(UInt16 id, Int16[] value)
        {
            CheckValue(id, DataTypes.Types.SINT16, value.Length);
            byte[] array = new byte[value.Length * 2];
            for (int i = 0; i < value.Length; i++)
            {
                SwitchEndianess(BitConverter.GetBytes(value[i])).CopyTo(array, i * 2);
            }
            SetBytes(id, array);
        }

        public void SetArray(UInt16 id, Int32[] value)
        {
            CheckValue(id, DataTypes.Types.SINT32, value.Length);
            byte[] array = new byte[value.Length * 4];
            for (int i = 0; i < value.Length; i++)
            {
                SwitchEndianess(BitConverter.GetBytes(value[i])).CopyTo(array, i * 4);
            }
            SetBytes(id, array);
        }

        public void SetArray(UInt16 id, Int64[] value)
        {
            CheckValue(id, DataTypes.Types.SINT64, value.Length);
            byte[] array = new byte[value.Length * 8];
            for (int i = 0; i < value.Length; i++)
            {
                SwitchEndianess(BitConverter.GetBytes(value[i])).CopyTo(array, i * 8);
            }
            SetBytes(id, array);
        }

        public void SetArray(UInt16 id, Half[] value)
        {
            CheckValue(id, DataTypes.Types.SFLOAT16, value.Length);
            byte[] array = new byte[value.Length * 2];
            for (int i = 0; i < value.Length; i++)
            {
                SwitchEndianess(BitConverter.GetBytes(value[i])).CopyTo(array, i * 2);
            }
            SetBytes(id, array);
        }

        public void SetArray(UInt16 id, float[] value)
        {
            CheckValue(id, DataTypes.Types.SFLOAT32, value.Length);
            byte[] array = new byte[value.Length * 4];
            for (int i = 0; i < value.Length; i++)
            {
                SwitchEndianess(BitConverter.GetBytes(value[i])).CopyTo(array, i * 4);
            }
            SetBytes(id, array);
        }

        public void SetArray(UInt16 id, double[] value)
        {
            CheckValue(id, DataTypes.Types.SFLOAT64, value.Length);
            byte[] array = new byte[value.Length * 8];
            for (int i = 0; i < value.Length; i++)
            {
                SwitchEndianess(BitConverter.GetBytes(value[i])).CopyTo(array, i * 8);
            }
            SetBytes(id, array);
        }

        #endregion
    }
}

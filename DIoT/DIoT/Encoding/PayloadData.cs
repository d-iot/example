﻿using DIoT.Schema;

namespace DIoT.Encoding
{
    internal class PayloadData
    {
        public DataTypes.Types Type;
        public int Array;

        public bool IsSet;
        public bool IsError;

        public byte[] Data = new byte[1];
        public bool Flag;

        public PayloadData(DataTypes.Types type, int array) 
        {
            Array = array;
            Type = type;

            if (type != DataTypes.Types.FLAG) 
            { 
                Data = new byte[DataTypes.SizeInBit(type)/8 * array];
            }
        }
    }
}

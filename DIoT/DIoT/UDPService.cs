﻿using System.Net.Sockets;

namespace DIoT
{
    public  class UDPService
    {
        private DIoTService _DIoTService;

        private CancellationTokenSource _CancellationTokenSource = new();

        public UDPService(DIoTService diotService) 
        {
            _DIoTService = diotService;
        }

        public void Start(int port)
        {
            Task.Run(async () =>
            {
                using (var udpClient = new UdpClient(port))
                {
                    while (true)
                    {
                        try
                        {
                            await _DIoTService.ProcessUDP(await udpClient.ReceiveAsync());
                        }
                        catch (Exception ex)
                        {

                        }
                        
                        if (_CancellationTokenSource.Token.IsCancellationRequested)
                        {
                            break;
                        }
                    }
                }
            });
        }

        public void Stop()
        {
            _CancellationTokenSource.Cancel();
        }
    }
}

﻿using DIoT.Encoding;
using DIoT.Encryption;
using DIoT.Header;
using DIoT.Storage;
using NLog;
using System.Net;
using System.Net.Sockets;

namespace DIoT
{
    public class DIoTService
    {
        private static Logger _Log = LogManager.GetCurrentClassLogger();

        private DeviceStorage _DeviceStorage;
        private SchemaStorage _SchemaStorage;
        private DataStorage _DataStorage;


        public DIoTService(DeviceStorage deviceStorage, SchemaStorage schemaStorage, DataStorage dataStorage)
        {
            _DeviceStorage = deviceStorage;
            _SchemaStorage = schemaStorage;
            _DataStorage = dataStorage;
        }

        public async Task ProcessUDP(UdpReceiveResult udpReceiveResult)
        {
            byte[] buffer = udpReceiveResult.Buffer;
            int bufferCounter = 0;

            if (buffer.Length == 0)
            {
                _Log.Debug("Recived empty UDP Packet");
                return;
            }

            byte[] baseHeaderBuffer = new byte[BaseHeader.GetHeaderSize()];
            Array.Copy(buffer, bufferCounter, baseHeaderBuffer, 0, BaseHeader.GetHeaderSize());
            BaseHeader baseHeader = new(baseHeaderBuffer);
            bufferCounter += BaseHeader.GetHeaderSize();

            switch (baseHeader.Type)
            {
                case Packtes.PacketTypes.Type.DATA:
                    await ProcessData(udpReceiveResult.RemoteEndPoint, buffer, bufferCounter).ConfigureAwait(false);
                    break;

                default:
                    _Log.Warn("Unsupported Packet Type");
                    break;
            }
        }

        public async Task ProcessData(IPEndPoint ipEndPoint, byte[] buffer, int bufferCounter)
        {
            byte[] dataHeaderBuffer = new byte[DataHeader.GetHeaderSize()];
            Array.Copy(buffer, bufferCounter, dataHeaderBuffer, 0, DataHeader.GetHeaderSize());
            DataHeader dataHeader = new(dataHeaderBuffer);
            bufferCounter += DataHeader.GetHeaderSize();

            if (!_DeviceStorage.KnowsDevice(dataHeader.DeviceAddress))
            {
                _Log.Debug("Unknown Device");
                return;
            }

            Device device = _DeviceStorage.GetDevice(dataHeader.DeviceAddress);

            if (device.MessageCounter > dataHeader.MessageCounter)
            {
                _Log.Debug("FrameCounter is lower");
                return;
            }

            byte[] encryptedPayload = new byte[buffer.Length - bufferCounter];
            Array.Copy(buffer, bufferCounter, encryptedPayload, 0, buffer.Length - bufferCounter);
            byte[] decryptedPayload;
            try
            {
                PayloadDecrypter payloadDecrypter = new PayloadDecrypter();
                decryptedPayload = payloadDecrypter.decrypt(device.Key, dataHeader.MessageCounter, dataHeaderBuffer, encryptedPayload);
            }
            catch
            {
                _Log.Debug("MAC is invaild");
                return;
            }
            await ProcessDataPayload(ipEndPoint, device, dataHeader.MessageCounter, decryptedPayload);
            _DeviceStorage.UpdateMessageCounter(dataHeader.DeviceAddress, dataHeader.MessageCounter + 1);
        }

        public async Task ProcessDataPayload(IPEndPoint ipEndPoint, Device device, UInt32 messageCounter, byte[] payload)
        {
            int payloadCounter = 0;

            while(payloadCounter < payload.Length)
            {
                byte[] schemaHeaderBuffer = new byte[SchemaHeader.GetHeaderSize()];
                Array.Copy(payload, payloadCounter, schemaHeaderBuffer, 0, SchemaHeader.GetHeaderSize());
                SchemaHeader schemaHeader = new(schemaHeaderBuffer);
                payloadCounter += SchemaHeader.GetHeaderSize();

                if(!device.Schemata.ContainsKey(schemaHeader.SchemaAddress))
                {
                    _Log.Debug("SchemaAddress unknown");
                    return;
                }

                if (!_SchemaStorage.KnowsSchema(device.Schemata[schemaHeader.SchemaAddress]))
                {
                    _Log.Debug("Schema unknown");
                    return;
                }
                Schema.Schema schema = _SchemaStorage.GetSchema(device.Schemata[schemaHeader.SchemaAddress]);
                PayloadDecoder payloadDecoder = new (schema);
                

                if (schemaHeader.HasError == false)
                {
                    if(payload.Length - payloadCounter < payloadDecoder.SchemaSize()) 
                    {
                        _Log.Debug("Payload smaller than schema");
                        return;
                    }

                    byte[] schemaPayload = new byte[payloadDecoder.SchemaSize()];
                    Array.Copy(payload, payloadCounter, schemaPayload, 0, payloadDecoder.SchemaSize());
                    payloadCounter += payloadDecoder.SchemaSize();

                    try
                    {
                        payloadDecoder.SetPayload(schemaPayload);
                        await _DataStorage.AddData(ipEndPoint, device, messageCounter, payloadDecoder);
                    }
                    catch
                    {
                        _Log.Warn("Payload invalid");
                        return;
                    }
                    
                }
                else
                {
                    if (payload.Length - payloadCounter < payloadDecoder.SchemaErrorByteSize())
                    {
                        _Log.Debug("Payload smaller than schema error");
                        return;
                    }

                    byte[] schemaError = new byte[payloadDecoder.SchemaErrorByteSize()];
                    Array.Copy(payload, payloadCounter, schemaError, 0, payloadDecoder.SchemaErrorByteSize());
                    payloadCounter += payloadDecoder.SchemaErrorByteSize();

                    int schemaSize = payloadDecoder.SchemaSizeError(schemaError);

                    if (payload.Length - payloadCounter < schemaSize)
                    {
                        _Log.Debug("Payload smaller than schema size with errors");
                        return;
                    }

                    byte[] schemaPayload = new byte[payloadDecoder.SchemaErrorByteSize() + schemaSize];
                    Array.Copy(payload, payloadCounter - payloadDecoder.SchemaErrorByteSize(), schemaPayload, 0, payloadDecoder.SchemaErrorByteSize() + schemaSize);
                    payloadCounter += schemaSize;

                    try
                    {
                        payloadDecoder.SetPayloadWithErrors(schemaPayload);
                        await _DataStorage.AddData(ipEndPoint, device, messageCounter, payloadDecoder);
                    }
                    catch
                    {
                        _Log.Warn("Payload invalid");
                        return;
                    }
                }
            }
        }
    }
}

﻿using System.Text.Json;

namespace DIoT.Storage
{
    public class DeviceStorage
    {
        private Dictionary<UInt32, Device> Devices = new ();

        public void LoadJSON(string json)
        {
            try
            {
                Devices = JsonSerializer.Deserialize<Dictionary<UInt32, Device>>(json) ?? throw new ArgumentException("JSON is invalid");
            }
            catch (Exception ex)
            {
                throw new ArgumentException("JSON is invalid", ex);
            }
        }

        public string ExportJSON()
        {
            JsonSerializerOptions options = new JsonSerializerOptions
            {
                WriteIndented = true
            };

            return JsonSerializer.Serialize(Devices, options);
        }

        public void AddDevice(UInt32 deviceAddress, Device device)
        {
            Devices.Add(deviceAddress, device);
        }

        public void RemoveDevice(UInt32 deviceAddress) 
        { 
            Devices.Remove(deviceAddress);
        }

        public bool KnowsDevice(UInt32 deviceAddress)
        {
            return Devices.ContainsKey(deviceAddress);
        }

        public Device GetDevice(UInt32 deviceAddress)
        {
            return Devices[deviceAddress];
        }

        public void IncreaseMessageCounter(UInt32 deviceAddress)
        {
            Devices[deviceAddress].IncreaseMessageCounter();
        }

        public void UpdateMessageCounter(UInt32 deviceAddress, UInt32 messageCounter)
        {
            Devices[deviceAddress].UpdateMessageCounter(messageCounter);
        }
    }
}

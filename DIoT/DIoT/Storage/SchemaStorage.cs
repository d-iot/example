﻿using DIoT.Schema;
using NLog;
using System.Collections.Generic;

namespace DIoT.Storage
{
    public class SchemaStorage
    {
        private static Logger _Log = LogManager.GetCurrentClassLogger();
        private Dictionary<byte[], Schema.Schema> _Schemata = new();

        public SchemaStorage() 
        { 
        
        }

        public void AddJSON(string json)
        {
            SchemaLoader schemaLoader = new SchemaLoader();

            try
            {
                schemaLoader.DeserializeJson(json);
            }
            catch
            {
                _Log.Warn("Invalid Schema JSON");
            }

            foreach(Schema.Schema s in schemaLoader.Schemata)
            {
                string validate = s.Validate();
                if(validate != string.Empty)
                {
                    _Log.Warn("Schema invalid");
                    continue;
                }

                byte[] uuid = Helper.ByteFromString(s.UUID);
                if (_Schemata.ContainsKey(uuid))
                {
                    _Log.Debug("Schema duplicate");
                    continue;
                }

                _Schemata.Add(uuid, s);
            }
        }

        public Schema.Schema GetSchema(byte[] uuid)
        {
            return _Schemata[_Schemata.Keys.Single(key => key.SequenceEqual(uuid))];
        }

        public bool KnowsSchema(byte[] uuid) 
        {
            return _Schemata.Keys.Any(key => key.SequenceEqual(uuid));
        }
    }
}

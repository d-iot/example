﻿using System.Text.Json.Serialization;

namespace DIoT.Storage
{
    public class Device
    {
        [JsonPropertyName("uuid")]
        public byte[] UUID { get; private set; }

        [JsonPropertyName("name")]
        public string Name { get; private set; }

        [JsonPropertyName("deviceaddress")]
        public UInt32 DeviceAddress { get; private set; }

        [JsonPropertyName("key")]
        public byte[] Key { get; private set; }

        [JsonPropertyName("messagecounter")]
        public UInt32 MessageCounter { get; private set; }

        [JsonPropertyName("schemata")]
        public Dictionary<byte, byte[]> Schemata { get; private set; }

        public Device(byte[] uuid, string name, UInt32 deviceAddress, byte[] key, UInt32 messageCounter, Dictionary<byte, byte[]> schemata)
        {
            UUID = uuid;
            Name = name;
            DeviceAddress = deviceAddress;
            Key = key;
            MessageCounter = messageCounter;
            Schemata = schemata;
        }

        public void IncreaseMessageCounter()
        {
            MessageCounter++;
        }

        public void UpdateMessageCounter(UInt32 messageCounter)
        {
            if (messageCounter <= MessageCounter)
            {
                throw new ArgumentException("MessageCounter is higher");
            }
            MessageCounter = messageCounter;
        }

        public void UpdateName(string name)
        {
            Name = name;
        }

        public void UpdateKey(byte[] key) 
        {
            Key = key;
            MessageCounter = 0;
        }
    }
}

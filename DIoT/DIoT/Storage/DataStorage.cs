﻿using DIoT.Encoding;
using System.Net;

namespace DIoT.Storage
{
    public class DataStorage
    {
        public Task AddData(IPEndPoint ipEndPoint, Device device, UInt32 messageCounter, PayloadDecoder payloadDecoder)
        {
            Console.WriteLine($"IP: {ipEndPoint}");
            Console.WriteLine($"Device: {device.DeviceAddress}");
            Console.WriteLine($"MessageID: {messageCounter}");
            Console.WriteLine($"Schema: {payloadDecoder.Schema.UUID}");

            for (int i = 0; i < payloadDecoder.Schema.Data.Count; i++)
            {
                Console.WriteLine($"Data [{i}]: Type - {payloadDecoder.Schema.Data[i].Type} = ???");
            }

            return Task.CompletedTask;
        }
    }
}

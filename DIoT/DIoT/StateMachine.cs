﻿using DIoT.Encoding;
using DIoT.Encryption;
using DIoT.Packtes;
using System.Net;

namespace DIoT
{
    public class StateMachine
    {
        public Guid Device;
        public UInt32 DeviceAddress;
        public State State;
        public UInt32 Message_Counter_Client;
        public UInt32 Message_Counter_Server;
        public byte[] Key;
        Dictionary<byte, Guid> Schemas;

        public StateMachine(Guid device, UInt32 deviceAddress)
        {
            Device = device;
            DeviceAddress = deviceAddress;
            State = State.INIT;
            Schemas = new Dictionary<byte, Guid>();
            Key = new byte[16];
        }

        public StateMachine(Guid device, State lastState, UInt32 deviceAddress, byte[] key, UInt32 message_Counter_Client, UInt32 message_Counter_Server, Dictionary<byte, Guid> schemas)
        {
            Device = device;
            State = lastState;
            DeviceAddress = deviceAddress;
            Key = key;
            Message_Counter_Client = message_Counter_Client;
            Message_Counter_Server = message_Counter_Server;
            Schemas = schemas;
        }

        public void ProcessHello(HelloPacket packet, IPEndPoint deviceIP)
        {
            throw new NotImplementedException();
        }

        public void ProcessData(DataPacket packet, IPEndPoint deviceIP)
        {
            if(Message_Counter_Client > packet.MessageCounter)
            {
                return; // Drop Packet
            }

            // TODO Validation of MAC and decrypt
            PayloadDecrypter payloadDecrypter = new PayloadDecrypter();

            try
            {
                List<byte> assossiatedData = new List<byte>();
                assossiatedData.AddRange(packet.BasePaket.GetHeader());
                assossiatedData.AddRange(BitConverter.GetBytes(packet.DeviceAddress));


                byte[] decryptedPayload = payloadDecrypter.decrypt(Key, Message_Counter_Client, assossiatedData.ToArray(), packet.Data);

                List<byte> payload = new List<byte>(decryptedPayload);

                while(payload.Count > 0)
                {
                    byte header = payload[0];
                    payload.RemoveAt(0);

                    byte schemaID = (byte)(header & (byte)0x7F);
                    bool hasError = header > 127;

                    if(!Schemas.ContainsKey(schemaID)) 
                    {
                        return; // Drop Packet
                    }
                    //PayloadDecoder payloadDecoder = new PayloadDecoder(Schemas[schemaID]);

                }

            }
            catch
            {
                return; // Drop Packet
            }
        }
    }
}

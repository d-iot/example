﻿using DIoT;
using DIoT.Storage;
using NLog;
using Org.BouncyCastle.Tls;

namespace DIoT_Server
{
    internal class Program
    {
        static int UDP_PORT = 29152;
        static string DeviceFile = "./devices2.json";
        static string SchemaFolder = "./schemata";

        private static Logger _Log = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logconsole = new NLog.Targets.ConsoleTarget("logconsole");
            config.AddRule(LogLevel.Debug, LogLevel.Fatal, logconsole);
            LogManager.Configuration = config;

            _Log.Info("Loading Device Storage");
            DeviceStorage deviceStorage = new();
            if(File.Exists(DeviceFile)) 
            {
                try
                {
                    deviceStorage.LoadJSON(File.ReadAllText(DeviceFile));
                }
                catch(Exception ex)
                {
                    _Log.Error(ex, "Loading Device failed");
                    return;
                }
            }

            Dictionary<byte, byte[]> schemata = new Dictionary<byte, byte[]>()
            {
                {2, Helper.ByteFromString("c8e4b56d-c51b-4f05-9ee9-c83914d73670") }
            };
            Device device = new Device(Helper.ByteFromString("2d464297-467e-4fc8-a99b-2cbeae730722"), "Test Device", 1001, Helper.ByteFromString("16352f16e9b04cdf9f2df669f60ffdb4"), 123, schemata);
            deviceStorage.AddDevice(1001, device);

            _Log.Info("Loading Schema Storage");
            SchemaStorage schemaStorage = new();
            if(!Directory.Exists(SchemaFolder)) 
            {
                _Log.Warn("Schemata folder is missing");
            }
            foreach(string s in Directory.GetFiles(SchemaFolder))
            {
                try
                {
                    schemaStorage.AddJSON(File.ReadAllText(s));
                }
                catch
                {
                    _Log.Error("Schema file invalid");
                }
            }

            _Log.Info("Loading Data Storage");
            DataStorage dataStorage = new();

            _Log.Info("Start DIoT Service");
            DIoTService diotService = new(deviceStorage, schemaStorage, dataStorage);

            _Log.Info("Start UDP Service");
            UDPService udpService = new(diotService);
            udpService.Start(UDP_PORT);

            _Log.Info("Server is running, press any key to quit");
            Console.ReadKey(true);

            _Log.Info("Stop UDP Service");
            udpService.Stop();

            _Log.Info("Update Device Storage");
            File.WriteAllText(DeviceFile, deviceStorage.ExportJSON());
        }
    }
}
﻿using DIoT.Schema;
using NUnit.Framework;

namespace DIoT_Test;

[TestFixture]
public class SchemaLoaderTest
{
    [Test]
    public void SerializeJsonCheck()
    {
        Schema schema = new Schema()
        {
            UUID = "308d7cdd-ba51-4168-a1ab-0321488180ac",
            Name = "NUnit Test",
            Description = "NUnit Test",
            Data = new List<Data>()
            {
                new()
                {
                    ID = 0,
                    Name = "Data1",
                    Description = "Desc1",
                    Type = DataTypes.Types.RAW,
                    Array = 16
                },
                new()
                {
                    ID = 1,
                    Name = "Data2",
                    Description = "Data3",
                    Type = DataTypes.Types.UINT8,
                    Enum = new Dictionary<string, double>()
                    {
                        { "ON", 0 },
                        { "OFF", 1 },
                        { "ERROR", 255 }
                    }
                }
            }
        };

        SchemaLoader testSchema = new SchemaLoader();
        testSchema.Schemata.Add(schema);

        string schemaJson = testSchema.SerializeJson();
        
        testSchema.DeserializeJson(schemaJson);
        
        Console.WriteLine(testSchema.SerializeJson());

        string errors = testSchema.Validate();
        Console.WriteLine(errors);
        Assert.AreEqual("", errors);
    }
}
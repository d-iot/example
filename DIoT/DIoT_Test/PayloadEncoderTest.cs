using DIoT;
using DIoT.Encoding;
using DIoT.Schema;
using NUnit.Framework;

namespace DIoT_Test;

[TestFixture]
public class PayloadEncoderTest
{
    [Test]
    public void Test()
    {
        string test1 = TestSchema.GetTest2();
        SchemaLoader schemaLoader = new SchemaLoader();
        schemaLoader.DeserializeJson(test1);

        Console.WriteLine(schemaLoader.Validate());
        PayloadEncoder pe1 = new PayloadEncoder(schemaLoader.Schemata[0]);

        pe1.SetValue(0, (byte)195);
        //pe1.SetValue(1, (UInt32)4278190335);
        pe1.SetError(1);
        pe1.SetValue(2, true);

        byte[] payload = pe1.GetPayloadWithErrors();

        Console.WriteLine(Helper.GetBitStringMSB(payload));

        PayloadDecoder pd1 = new PayloadDecoder(schemaLoader.Schemata[0]);
        pd1.SetPayloadWithErrors(payload);

        Console.WriteLine(pd1.GetValueByte(0));
        //Console.WriteLine(pd1.GetValueUInt32(1));
        Console.WriteLine(pd1.IsError(1));
        Console.WriteLine(pd1.GetValueBool(2));
    }
}
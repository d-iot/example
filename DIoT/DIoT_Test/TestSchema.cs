namespace DIoT_Test;

public static class TestSchema
{
    public static string GetTest1()
    {
        string fileName = "./Schema/Test1.diot";
 
        return File.ReadAllText(fileName);
    }

    public static string GetTest2()
    {
        string fileName = "./Schema/Test2.diot";

        return File.ReadAllText(fileName);
    }
}
import socket
import time

UDP_PORT = 29152
UDP_BUFFER = 1025

SERVER_IP4 = "127.0.0.1"
SERVER_IP6 = "::/0"

if __name__ == '__main__':
    
    sock4 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    counter = 0
    while True:
        print("Send Packet: {}".format(counter))
        sock4.sendto(bytes("TEST", "utf-8"), (SERVER_IP4, UDP_PORT))
        counter = counter + 1
        time.sleep(2)
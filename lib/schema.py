class DIoT_Schema:
    def __init__(self, uuid, name, description, data):
        self.uuid = uuid
        self.name = name
        self.description = description
        self.data = data

class DIoT_Range:
    def __init__(self, min, max):
        self.min = min
        self.max = max
        
class DIoT_Data:
    def __init__(self, id, name, description, type, array, factor, offset, unit, range, enum):
        self.id = id
        self.name = name
        self.description = description
        self.type = type
        self.array = array
        self.factor = factor
        self.offset = offset
        self.unit = unit
        self.range = range
        self.enum = enum
        self.value = None